import os, glob
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Process some files.')
parser.add_argument('--directory', type=str, help='Location of the files on Jean Zay commons', required=True)
parser.add_argument('--section', type=str, help='Either head or tail', required=True)

args = parser.parse_args()

# Pass the variables to the script
directory = args.directory
section = args.section

list_files = glob.glob(directory + "/*parquet")
length_files = len(list_files)
limitation = round(length_files/2)

if section == "head":
    list_files = list_files[0:limitation]
else:
    list_files = list_files[limitation:length_files+1]

complete_set = []

for file in list_files:
    content = pd.read_parquet(file)
    complete_set.append(content)

complete_set = pd.concat(complete_set)

if section == "head":
    complete_set.to_parquet("/gpfsdswork/projects/rech/fmr/uft12cr/corpus/ocr_sample/" + os.path.basename(directory) + "_1.parquet")
else:
    complete_set.to_parquet("/gpfsdswork/projects/rech/fmr/uft12cr/corpus/ocr_sample/" + os.path.basename(directory) + "_2.parquet")

