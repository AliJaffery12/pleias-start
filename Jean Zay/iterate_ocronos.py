import os, re
import pandas as pd
from pprint import pprint
import argparse
import os.path
import regex
from vllm import LLM, SamplingParams
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    BitsAndBytesConfig,
    HfArgumentParser,
    TrainingArguments,
    pipeline,
    logging,
    LlamaTokenizerFast
)
import copy
import re
from pathlib import Path

#Typical usage: python3 iterate_ocronos.py --directory "/gpfsdswork/dataset/HuggingFace/PleIAs/French-PD-Newspapers" --file "gallica_presse_1.parquet" --output_directory "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/french_newspaper_ocr" --chunk 10 --start 0 --end 2

# Define argparse to accept command-line arguments
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--file', type=str, help='File name', required=True)
parser.add_argument('--directory', type=str, help='Location of the files on Jean Zay commons', required=True)
parser.add_argument('--output_directory', type=str, help='Location for the output files', required=True)
parser.add_argument('--chunk', type=int, help='Start batch number', required=True)
parser.add_argument('--start', type=int, help='Start batch number', required=True)
parser.add_argument('--end', type=int, help='End batch number', required=True)

args = parser.parse_args()

# Pass the variables to the script
directory = args.directory
file = args.file
start_batch = args.start
end_batch = args.end
output_directory = args.output_directory
chunk_size = args.chunk #Taking chunks of 10 documents to send to vllm. I prefer to do a lot in case there are some interruptions
simple_file_name = file.replace(".parquet", "")
result_directory = output_directory + "/" + simple_file_name

Path(result_directory).mkdir(parents=True, exist_ok=True)

def clean_text(text):
    # First, remove all leading newlines and whitespace
    text = text.lstrip()
    
    # Now, remove leading punctuation using a regular expression
    text = re.sub(r'^\W+', '', text)

    text = re.sub(' +', ' ', text)
    
    return text

# Helper function to split text at the last occurrence of pattern not exceeding max_words
def split_at_pattern(text, pattern, max_words):
    chunks = []
    remaining_text = text
    while remaining_text:
        split_point = -1
        for m in re.finditer(pattern, remaining_text):
            words_up_to_match = len(re.findall(r'\S+', remaining_text[:m.end()]))
            if words_up_to_match > max_words:
                break
            split_point = m.end()
        if split_point == -1 or len(re.findall(r'\S+', remaining_text)) <= max_words:
            chunks.append(remaining_text)
            break
        chunks.append(remaining_text[:split_point])
        remaining_text = remaining_text[split_point:]
    return chunks

def split_text_into_chunks(text, min_words=1000, absolute_max_words=1500):
    
    # Initial split trying to adhere to the ideal max words and ending with a period followed by newline
    primary_chunks = split_at_pattern(text, r'\.\n', absolute_max_words)

    # Further split any chunks that are too long, using just a period as the split point
    secondary_chunks = []
    for chunk in primary_chunks:
        if len(re.findall(r'\S+', chunk)) > absolute_max_words:
            secondary_chunks.extend(split_at_pattern(chunk, r'\.', min_words))
        else:
            secondary_chunks.append(chunk)

    # If any chunks are still too long, forcefully split them without considering the pattern
    final_chunks = []
    for chunk in secondary_chunks:
        if len(re.findall(r'\S+', chunk)) > absolute_max_words:
            words = re.findall(r'\S+', chunk)
            for i in range(0, len(words), min_words):
                final_chunks.append(' '.join(words[i:i+min_words]))
        else:
            final_chunks.append(chunk)
            
    return final_chunks

sample_parquet = pd.read_parquet(directory + "/" + file)

#Special case for some corpora
if 'complete_text' in sample_parquet.columns:
    sample_parquet.rename(columns={'complete_text': 'text'}, inplace=True)

# List to hold the sub datasets
sub_datasets = [sample_parquet[i:i + chunk_size] for i in range(0, sample_parquet.shape[0], chunk_size)]

#Loading the LLM
llm = LLM("/gpfsdswork/projects/rech/fmr/uft12cr/ocr/mistral-ocr-large-3/mistral-ocr-large-3", max_model_len=8128)
sampling_params = SamplingParams(temperature=0.9, top_p=.95, max_tokens=4000, presence_penalty = 0, stop = ["#END#"])

dataset_set_id = start_batch
for dataset in sub_datasets[start_batch:end_batch]:

    print(dataset)
    list_index = []
    list_chunks = []

    for index, row in dataset.iterrows():
        row_dict = row.to_dict()
        text = row_dict["text"]
        chunks = split_text_into_chunks(text)

        for chunk in chunks:
            chunk = clean_text(chunk) #Removing newlines/punctuation at the start since they will mess with initialization.
            list_chunks.append(chunk)
            list_index.append(index)
    
    #We are going the first index row as the name for our dataset
    index_name = list_index[0]

    result_file = f"{result_directory}/{simple_file_name}_{index_name}.parquet"

    if not os.path.isfile(result_file):
        correction_selection = pd.DataFrame(
            {'index_id': list_index,
            'chunks': list_chunks
            })
        
        print(correction_selection)

        list_chunks = []

        for chunk in correction_selection["chunks"]:
            correction = f"### TEXT ###\n{chunk}\n\n### CORRECTION ###\n"
            list_chunks.append(correction)

        outputs = llm.generate(list_chunks, sampling_params)

        output_list = []
        id_output = 0
        for output in outputs:
            generated_text = output.outputs[0].text
            output_list.append(generated_text)
            id_output = id_output+1

        correction_selection["corrected_text"] = output_list

        print(correction_selection)

        correction_selection = correction_selection.groupby('index_id')['corrected_text'].apply(lambda x: '\n'.join(x)).reset_index()

        dataset = dataset.reset_index()

        # Rename the 'index' column to 'index_id' to match the first DataFrame
        dataset.rename(columns={'index': 'index_id'}, inplace=True)

        merged_df = pd.merge(dataset, correction_selection, on='index_id')

        print(merged_df)

        merged_df.to_parquet(result_file)

        dataset_set_id = dataset_set_id + 1 #We update the id
