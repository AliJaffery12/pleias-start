import os, time
import argparse

#Special python file to execute multiple jobs in the same time (so long as we can do that)
#Since this is a long query, to execute with nohup

#nohup python3 vllm_orchestration.py --start_job 1 --end_job 40 &

#First we import the slurm template file
slurm_template = open("vllm_template.slurm", 'r').read()

parser = argparse.ArgumentParser(description='Process some jobs.')
parser.add_argument('--start_job', type=int, help='Start batch job', required=True)
parser.add_argument('--end_job', type=int, help='End batch job', required=True)

args = parser.parse_args()

start_job = args.start_job
end_job = args.end_job

for current_job in range(start_job, end_job):

    slurm_template = slurm_template.replace("pleias_n", "pleias_" + str(current_job))

    start_batch = (current_job*10)-10
    end_batch = (current_job*10)

    #We define the suffix

    #suffix = """python3 iterate_ocronos.py --directory "/gpfsdswork/dataset/HuggingFace/PleIAs/German-PD" --file "german_pd_1.parquet" --output_directory "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/german_ocr" --chunk 3 --start """ + str(start_batch) + """ --end """ + str(end_batch)

    #suffix = """python3 iterate_ocronos.py --directory "/gpfsdswork/dataset/HuggingFace/PleIAs/Italian-PD" --file "italian_pd_1.parquet" --output_directory "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/italian_ocr" --chunk 5 --start """ + str(start_batch) + """ --end """ + str(end_batch)

    #suffix = """python3 iterate_ocronos.py --directory "/gpfsdswork/dataset/HuggingFace/PleIAs/US-PD-Newspapers" --file "dlc_laceflower_ver01.parquet" --output_directory "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/english_ocr" --chunk 50 --start """ + str(start_batch) + """ --end """ + str(end_batch)

    suffix = """python3 iterate_ocronos.py --directory "/gpfsdswork/dataset/HuggingFace/PleIAs/French-PD-Newspapers" --file "gallica_presse_5.parquet" --output_directory "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/french_newspaper_ocr" --chunk 10 --start """ + str(start_batch) + """ --end """ + str(end_batch)

    slurm_command = slurm_template + " " + suffix

    print(slurm_command)

    with open("temporary.slurm", 'w') as temp_slurm:
        temp_slurm.write(slurm_command)

    time.sleep(1)

    os.system("sbatch temporary.slurm")

    time.sleep(600) #We wait 10 minutes between jobs to not overload Jean Zay with queries