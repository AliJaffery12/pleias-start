import pandas as pd
from vllm import LLM, SamplingParams
import os
from pprint import pprint

#Needed to run vllm offline
os.environ['TRANSFORMERS_OFFLINE'] = '1'


def get_first_hermes_response(user_input):
  detailed_prompt = """<|im_start|>system\nTu es un assistant conversationnel français. Donne des réponses utiles, détaillées et polies à ton interlocuteur.<|im_end|>\n<|im_start|>user"""
  user_input = "Reformule le paragraphe suivant en utilisant un langage concis et abscon mais toujours en langue française, que seul un chercheur érudit serait en mesure de comprendre. Remplace les mots et les phrases simples par des mots et des phrases complexes, voire rares :\n" + user_input
  detailed_prompt = detailed_prompt + "\n" + user_input + "<|im_end|>\n<|im_start|>assistant\n"
  return detailed_prompt

text_book_content = pd.read_json("textbook_test.json")

prompts = []
for index, row in text_book_content.iterrows():
  final_text = get_first_hermes_response(row["text_book"])
  prompts.append(final_text)

llm = LLM(model="/gpfsdswork/projects/rech/fmr/uft12cr/vllm_run/mistral-hermes", tensor_parallel_size=4)
sampling_params = SamplingParams(temperature=0.4, top_p=0.95, max_tokens=1000, presence_penalty = 2)

outputs = llm.generate(prompts, sampling_params, use_tqdm = False)

list_generated = []
# Print the outputs.
for output in outputs:
    prompt = output.prompt
    generated_text = output.outputs[0].text
    list_generated.append(generated_text)

text_book_content["generated"] = list_generated
text_book_content.to_json("result.json", orient='records')

