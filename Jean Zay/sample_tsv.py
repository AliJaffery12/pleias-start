import pandas as pd
import random
import argparse

parser = argparse.ArgumentParser(description='Process some files.')
parser.add_argument('--file', type=str, help='Either head or tail', required=True)

args = parser.parse_args()

# Pass the variables to the script
file = args.file

source_file = "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/ocr_sample/" + file

# Load the parquet file
df = pd.read_parquet(source_file)

complete_text = df["text"].tolist()

max_size = 700

new_text = []
for text in complete_text:
    text = text[0:+max_size]
    new_text.append(text)

df["text"] = new_text

complete_text = df["corrected_text"].tolist()

max_size = 700

new_text = []
for text in complete_text:
    text = text[0:+max_size]
    new_text.append(text)

df["corrected_text"] = new_text

target_file = "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/ocr_sample_tsv/" + file.replace(".parquet", ".tsv")

df.to_csv(target_file, sep = "\t")