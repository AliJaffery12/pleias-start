import os
import pandas as pd
import argparse


parser = argparse.ArgumentParser(description='Process some jobs.')
parser.add_argument('--directory', type=str, help='Directory name', required=True)
args = parser.parse_args()

def process_parquet_files(input_dir, output_file):
    # Initialize a list to store DataFrame chunks
    df_list = []   

    count_files = 0

    # Iterate over all the parquet files in the directory
    for filename in os.listdir(input_dir):
        count_files = count_files + 1
        if filename.endswith('.parquet'):
            file_path = os.path.join(input_dir, filename)
            
            # Read only the necessary columns, modify this according to your needs
            cols_to_read = ['word_count']  # Specify your required columns
            df = pd.read_parquet(file_path, columns=cols_to_read)

            # Append the processed DataFrame to the list
            df_list.append(df)

    # Concatenate all DataFrame chunks
    metadata_df = pd.concat(df_list, ignore_index=True)

    # Write the new dataset to a unique parquet file
    #metadata_df.to_parquet(output_file)

    print(metadata_df)

    metadata_df = metadata_df.astype(int)
    word_count = metadata_df[["word_count"]].sum()
    word_count_per_file = word_count/count_files
    estimated_total_count = word_count_per_file*10

    print("Word Count: " + str(word_count))
    print("Word Count per file: " + str(word_count_per_file))
    print("Estimated gloabl count: " + str(estimated_total_count))


# Directory containing the Parquet files
input_dir = args.directory
# Output file path
output_file = ''

process_parquet_files(input_dir, output_file)
