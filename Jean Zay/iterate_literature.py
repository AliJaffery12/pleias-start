import os, re
import pandas as pd
from pprint import pprint
import argparse
import regex
from vllm import LLM, SamplingParams
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    BitsAndBytesConfig,
    HfArgumentParser,
    TrainingArguments,
    pipeline,
    logging,
    LlamaTokenizerFast
)
import copy
from pathlib import Path

source_file = pd.read_json("english_fiction_set_83.json", sep="\t")

#Loading the LLM
llm = LLM("/gpfsdswork/projects/rech/fmr/uft12cr/literature/llama-notte-inverno/llama-notte-inverno", max_model_len=8128)
sampling_params = SamplingParams(temperature=0.9, top_p=.95, max_tokens=4000, presence_penalty = 0)

list_analysis = []
for index, row in source_file.iterrows():
    row_dict = row.to_dict()
    text = row_dict["text"]
    correction = f"### TEXT ###\n{text}\n\n### ANALYSIS ###\n"
    list_analysis.append(correction)
    
outputs = llm.generate(list_analysis, sampling_params)

output_list = []
id_output = 0
for output in outputs:
    generated_text = output.outputs[0].text
    output_list.append(generated_text)
    id_output = id_output+1

source_file["analysis"] = output_list

list_rephrasing = []
for index, row in source_file.iterrows():
    row_dict = row.to_dict()
    analysis = row_dict["analysis"]
    correction = f"### ANALYSIS ###\n{analysis}\n\n### TEXT ###\n"
    list_rephrasing.append(correction)
    
outputs = llm.generate(list_rephrasing, sampling_params)

output_text = []
id_output = 0
for output in outputs:
    generated_text = output.outputs[0].text
    output_text.append(generated_text)
    id_output = id_output+1

source_file["rephrasing"] = output_text

source_file.to_csv("export.tsv", sep="\t")
