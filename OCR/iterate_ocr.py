import os, re
import pandas as pd
from pprint import pprint
from vllm import LLM, SamplingParams
import copy

def prepare_segments(text, max_words = 800):
    lines = text.split("\n")
    list_segment = []
    new_segment = []
    count_word = 0
    for line in lines:
        n_words = len(line.split())
        count_word += n_words
        if re.match("^[A-Z][A-Z]", line):
            if count_word >= max_words:
                count_word = 0
                list_segment.append(" ".join(new_segment))
                new_segment = [line]
        else:
            new_segment.append(line)
    return list_segment

sample_parquet = pd.read_parquet("sample_ca.parquet").head(5)

def get_first_hermes_response(user_input, mode = "interview"):
  sampling_params = SamplingParams(temperature=0.7, top_p=.95, max_tokens=2000, presence_penalty = 0, stop=["#END#"])
  detailed_prompt = """Text:"""
  detailed_prompt = detailed_prompt + " " + user_input + "\n\n### Correction:\n\n"
  prompts = [detailed_prompt]
  outputs = llm.generate(prompts, sampling_params, use_tqdm = False)
  generated_text = outputs[0].outputs[0].text
  prompt = detailed_prompt + generated_text
  return prompt, generated_text


llm = LLM("mistral-ocr/mistral-ocr")

sampling_params = SamplingParams(temperature=0.2, top_p=0.95, max_tokens=500)

corpus_segment = []
list_corrections = []
for index, row in sample_parquet.iterrows():
    row_dict = row.to_dict()
    text = row_dict["text"]
    list_segment = prepare_segments(text)
    for segment in list_segment:
        new_row = copy.deepcopy(row_dict)
        
        new_row["text"] = segment
        correction = "Text: " + " " + user_input + "\n\n### Correction:\n\n"
        list_corrections.append(correction)
        corpus_segment.append(new_row)

corpus_segment = pd.DataFrame.from_records(corpus_segment)

outputs = llm.generate(prompts, sampling_params, use_tqdm = False)

output_list = []
for output in outputs:
    generated_text = output.outputs[0].text
    output_list.append(generated_text)

corpus_segment["correction"] = output_list

print(corpus_segment)

list_correction.to_json("test_chronicling.json")
