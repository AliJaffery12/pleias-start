import os, re
import pandas as pd
from pprint import pprint
from vllm import LLM, SamplingParams
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    BitsAndBytesConfig,
    HfArgumentParser,
    TrainingArguments,
    pipeline,
    logging,
    LlamaTokenizerFast
)
import copy

#sample_parquet = pd.read_parquet("sample_ca.parquet").head(5)

sample_parquet = pd.read_json("ocr_data_test.json")

print(sample_parquet)

#llm = LLM("mistral-ocr-DPO", max_model_len=8129)

#self.sampler = Sampler(config.tokenizer_vocab_size)
#llm = LLM("mistral-ocr/mistral-ocr", max_model_len=8128)
llm = LLM("mistral-ocr-large-3/mistral-ocr-large-3", max_model_len=8128)
#llm = LLM("mistral-ocr-DPO-2", max_model_len=8128)

sampling_params = SamplingParams(temperature=0.9, top_p=.95, max_tokens=4000, presence_penalty = 0, stop = ["#END#"])
#sampling_params = SamplingParams(temperature=0.7, top_p=.95, max_tokens=4000, presence_penalty = 0)

print("Starting with training")

corpus_segment = []
list_corrections = []
list_initialization = []
for index, row in sample_parquet.iterrows():
    row_dict = row.to_dict()
    text = row_dict["text"]
    initialization = text.split()[0] + " "
    #correction = """Text: """ + text + " ### Correction: " + initialization
    correction = f"### TEXT ###\n{text}\n\n### CORRECTION ###\n"
    list_initialization.append(initialization)
    #correction = "<|im_start|>system\nCorrect the OCR errrors in this text<|im_end|>\n<|im_start|>user\n" + text + "<|im_end|>\n<|im_start|>assistant\n"
    print(correction)
    list_corrections.append(correction)

outputs = llm.generate(list_corrections, sampling_params)

output_list = []
id_output = 0
for output in outputs:
    generated_text = output.outputs[0].text
    generated_text = generated_text.replace("\n\n", "")
    output_list.append(generated_text)
    #output_list.append(generated_text)
    id_output = id_output+1

sample_parquet["correction"] = output_list

print(sample_parquet)

sample_parquet.to_json("test_ocr_base_initialization.json", orient = "records")