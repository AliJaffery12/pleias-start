import pandas as pd
from vllm import LLM, SamplingParams
import os

#Needed to run vllm offline
os.environ['TRANSFORMERS_OFFLINE'] = '1'


def get_first_hermes_response(user_input, mode = "interview"):
  sampling_params = SamplingParams(temperature=0.4, top_p=.95, max_tokens=500, presence_penalty = 2)
  detailed_prompt = """<|im_start|>system\nTu es Bellay, le chatbot de la culture et de la littérature française.<|im_end|>\n<|im_start|>user"""
  detailed_prompt = detailed_prompt + "\n" + user_input + "<|im_end|>\n<|im_start|>assistant\n"
  prompts = [detailed_prompt]
  outputs = llm.generate(prompts, sampling_params, use_tqdm = False)
  generated_text = outputs[0].outputs[0].text
  prompt = detailed_prompt + generated_text
  return prompt, generated_text

llm = LLM(model="llama-8b-instruct")

print("Model loaded")
sampling_params = SamplingParams(temperature=0.4, top_p=0.95, max_tokens=500)

print("Parameters loaded")

max_exchange = 20
claire_mode = "politique"

user_input = "Bonjour Bellay. Ecrit moi une histoire drôle!"

print("Inputs loaded")

prompt, generated_text = get_first_hermes_response(user_input, mode = claire_mode)

with open("test.txt", 'w') as text:
    text.write(prompt)
