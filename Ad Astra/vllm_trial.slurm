#!/bin/bash
#SBATCH --job-name=warpx
#SBATCH --account=c1614990
#SBATCH --constraint=MI250
#SBATCH --ntasks-per-node=8 --cpus-per-task=8 --gpus-per-node=8
#SBATCH --threads-per-core=1 # --hint=nomultithread
#SBATCH --exclusive
#SBATCH --output=%x-%j.out
#SBATCH --time=00:40:00
#SBATCH --nodes=1

set -eu

# Cloning the code on a login node.
#git clone https://github.com/vllm-project/vllm || true

# Building the code on a compute node and installing it in the current Python environment.
module purge

module load cpe/23.12
module load craype-accel-amd-gfx90a craype-x86-trento
module load PrgEnv-gnu
module load amd-mixed/5.7.1 rocm/5.7.1
module load cray-python

module list

# Source a VEnv if needed.
module load conda/22.9.0
conda activate myenv

NEW_ROCM_PATH="$(pwd)/$(basename -- "${ROCM_PATH}")"

if [[ ! -e "${NEW_ROCM_PATH}" ]]; then
    cp -rv "$(
        cd -- "${ROCM_PATH}" >/dev/null 2>&1
        pwd -P
    )" "${NEW_ROCM_PATH}"
fi

export ROCM_PATH="${NEW_ROCM_PATH}"
export PATH="${ROCM_PATH}/bin:${PATH}"
export HIP_LIB_PATH="${ROCM_PATH}/lib"
export LD_LIBRARY_PATH="${HIP_LIB_PATH}:${LD_LIBRARY_PATH}"
export CMAKE_PREFIX_PATH="${ROCM_PATH}/lib/cmake:${CMAKE_PREFIX_PATH}"
export HSAKMT_DEBUG_LEVEL=4
export ROCM_LOG_LEVEL=4

PYTORCH_ROCM_ARCH="gfx90a" python vllm_trial.py